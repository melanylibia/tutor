

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TimerPlayer : MonoBehaviour
{
    public TextMeshProUGUI timerText;
    private float startTime;
    private int min;
    private int sec;
    private float totalTime;
    public static TimerPlayer inst;


    public float TotalTime
    {
        get { return totalTime; }
        set { totalTime = value; }
    }
    private bool continuar = true;
    public bool Continuar
    {
        get { return continuar; }
        set { continuar = value; }
    }
    float currentTime = 0.0f;
    bool isPaused = false;
    bool isStoped = false;
    void Start()
    {
        inst = this;
        timerText = GetComponent<TextMeshProUGUI>();
        Setup();
        currentTime = 0.0f;
        timerText.text ="0:0";
    }
    void OnEnable()
    {
        timerText = GetComponent<TextMeshProUGUI>();
        Setup();
    }
    public void Setup()
    {
        setupTime();
        isPaused = false;
        isStoped = false;
        continuar = true;
    }
    public void setupTime()
    {
        min = 0;
        sec = 0;
        startTime = Time.time;
        currentTime = 0.0f;
    }
    
    public void pausarTimer()
    {
        isPaused = !isPaused;
        if (!isPaused) { isStoped = false; }
    }
    int t = 0;
    void Update()
    {
        if (!continuar) return;
        if (isStoped) return;
        if (!isPaused)
        {
            if (t < 30)
            {
                t+=1;
            }
            else {
                t = 0;
                currentTime = currentTime + 1.0f;
                totalTime = currentTime;
                min = (int)currentTime / 60;
                string minutes = min.ToString();
                sec = (int)(currentTime % 60);
                string seconds = sec.ToString();
                timerText.text = minutes + ":" + seconds;
            }
        }
        else
        {
            isStoped = true;
        }
    }

    public int getTotalTime()
    {
        return (min * 60 + sec);
    }
}