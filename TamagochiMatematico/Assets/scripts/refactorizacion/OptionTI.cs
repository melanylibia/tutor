using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OptionTI : MonoBehaviour
{
    private int _value;
    public int _Value
    {
        get { return _value; }
        set {_value  = value; }
    }
    

    // void Start()
    // {
    //     GetComponent<Button>().onClick.AddListener(submit);
    // }
    private void OnEnable() {
        GetComponent<Button>().onClick.AddListener(submit);
    }
    private void OnDisable() {
        GetComponent<Button>().onClick.RemoveAllListeners();
    }
    public void submit()
    {
        TestInicial.testInicial.valorOption(_value);
    }
}


