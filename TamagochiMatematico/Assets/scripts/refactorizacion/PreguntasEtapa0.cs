using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using UnityEngine.UI;
using System;
public class PreguntasEtapa0 : MonoBehaviour
{
    public Texture texture_inicial;
    int nroCorrect = 0;
    bool isEnd = false;
    public GameObject obj_pregunta;
    TextMeshProUGUI pregunta;
    public RawImage preguntaImg;
    public TextMeshProUGUI option1;
    public TextMeshProUGUI option2;
    public int index = 0;
    public int limitQuestions = 5;
    private List<string> repasoList;
    public GameObject screenEndQuiz;
    public GameObject timerObject;
    public static event Action OnActionPlayerCorrectAnswer;
    public Transform trans_opt1;
    public Transform trans_opt2;

    void OnEnable()
    {
        repasoList = new List<string>();
        timerObject.GetComponent<TimerPlayer>().Setup();
        screenEndQuiz.SetActive(false);
        index = 0;
        isEnd = false;
        nroCorrect = 0;
        if (data != null && data.datos[index].esImagen)
        { preguntaImg.texture = texture_inicial; preguntaImg.gameObject.SetActive(true); }
        else { obj_pregunta.gameObject.SetActive(true); }
        option1.transform.parent.gameObject.SetActive(true);
        option2.transform.parent.gameObject.SetActive(true);
    }
    void Awake()
    {
        pregunta = obj_pregunta.GetComponentInChildren<TextMeshProUGUI>();
        texture_inicial = preguntaImg.texture;
        option1.GetComponentInParent<Options>().PosicionInicial = trans_opt1;
        option2.GetComponentInParent<Options>().PosicionInicial = trans_opt2;
    }
    void Start()
    {
        index = 0;
        isEnd = false;
        nroCorrect = 0;
        repasoList = new List<string>();
    }

    MenuManagerQuiz.ResponseData data;

    public void submitAnswer(MenuManagerQuiz.ResponseData newData)
    {
        this.data = newData;
        establecerPreguntaUI();
    }

    public void submitAnswerQuiz(bool isCorrect)
    {
        if (isEnd) return;

        if (isCorrect)
        {
            if (OnActionPlayerCorrectAnswer != null)
            {
                OnActionPlayerCorrectAnswer.Invoke();
            }
            nroCorrect++;
        }
        else
        {
            repasoList.Add(data.datos[index].apoyo);
        }

        if (index < data.datos.Count - 2)
        {
            index++;
            establecerPreguntaUI();
        }
        else
        {
            isEnd = true;
            if (timerObject)
            {
                RespuestasQuiz.inst.endQuiz(nroCorrect.ToString(), timerObject.GetComponent<TimerPlayer>().TotalTime.ToString());
                EndQuizUI();
            }
        }
    }
    public void EndQuizUI()
    {
        screenEndQuiz.SetActive(true);
        MenuManagerQuiz.inst.map.SetActive(true);
        if (repasoList.Count != 0)
        {
            screenEndQuiz.GetComponent<EndQuiz>().PlayAgain();
            screenEndQuiz.GetComponent<EndQuiz>().setEndQuiz(repasoList);
        }
        else
        {
            screenEndQuiz.GetComponent<EndQuiz>().NextLesson();
            screenEndQuiz.GetComponent<EndQuiz>().setMessage();
        }
        clearScreen();
    }
    public void establecerPreguntaUI()
    {
        if (data != null && !data.datos[index].esImagen)
        {
            if (option1 && option2 && pregunta)
            {
                preguntaImg.gameObject.SetActive(false);
                obj_pregunta.gameObject.SetActive(true);
                pregunta.text = data.datos[index].texto;
                option1.GetComponentInParent<Options>().IsCorrect = data.datos[index].respuestas[0].esCorrecta;
                option2.GetComponentInParent<Options>().IsCorrect = data.datos[index].respuestas[1].esCorrecta;
                option1.text = data.datos[index].respuestas[0].texto;
                option2.text = data.datos[index].respuestas[1].texto;
            }
        }
        else
        {
            preguntaImg.texture = texture_inicial;
            preguntaImg.gameObject.SetActive(true);
            obj_pregunta.gameObject.SetActive(false);
            StartCoroutine(setImagePregunta(data.datos[index].texto));
            option1.GetComponentInParent<Options>().IsCorrect = data.datos[index].respuestas[0].esCorrecta;
            option2.GetComponentInParent<Options>().IsCorrect = data.datos[index].respuestas[1].esCorrecta;
            option1.text = data.datos[index].respuestas[0].texto;
            option2.text = data.datos[index].respuestas[1].texto;
        }
    }
    public static event Action<int, string> OnActionImagenPregunta;
    IEnumerator setImagePregunta(string imageUrl)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(imageUrl);
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
        {
            // MenuManagerQuiz.inst.mostrarMensaje(0,"No se pudo obtener la imagen de la pregunta");
            if (OnActionImagenPregunta != null)
            {
                OnActionImagenPregunta.Invoke(0, "No se pudo obtener la imagen de la pregunta");
            }
            Debug.LogError("Error loading image: " + www.error);
        }
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            preguntaImg.texture = texture;
        }
    }
    public void clearScreen()
    {
        if (data != null && !data.datos[index].esImagen)
        {
            pregunta.text = "";
        }
        else
        {
            preguntaImg.texture = texture_inicial;
        }

        option2.text = "";
        option1.text = "";
        timerObject.GetComponent<TimerPlayer>().Continuar = false;
        timerObject.GetComponent<TimerPlayer>().timerText.text = "";
        obj_pregunta.gameObject.SetActive(false);
        option1.transform.parent.gameObject.SetActive(false);
        option2.transform.parent.gameObject.SetActive(false);
        preguntaImg.gameObject.SetActive(false);
    }
}