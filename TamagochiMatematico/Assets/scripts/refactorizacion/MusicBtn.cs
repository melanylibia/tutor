using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MusicBtn : MonoBehaviour
{
    void Awake()
    {
        AudioGame.audioGame.current_music_sound = GetComponent<Image>();
        updateAudio();
    }
    public void updateAudio()
    {
        if (MainGameController.inst.IsSoundPaused == false)
        {
            AudioGame.audioGame.current_music_sound.sprite = AudioGame.audioGame.music_sound_on;
        }
        else
        {
            AudioGame.audioGame.current_music_sound.sprite = AudioGame.audioGame.music_sound_off;
        }
    }
    
    public void pausar()
    {
        AudioGame.audioGame.Pausar();
    }
}
