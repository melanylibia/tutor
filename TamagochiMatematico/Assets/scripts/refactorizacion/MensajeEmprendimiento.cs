using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
public class MensajeEmprendimiento : MonoBehaviour
{

    [System.Serializable]
    public class MsgData
    {
        public bool finalizado;
        public MsgDataData datos;

        [System.Serializable]
        public class MsgDataData
        {
            public string mensaje;
        }
    }

    IEnumerator getMsgEmprendimiento()
    {
        string url = "https://tutor-api.hacklab.org.bo/api/mensajes/mensajeAleatorio";
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("Authorization", "Bearer " + Player.inst.UserToken);
        
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            MenuManagerQuiz.inst.mostrarMensaje(0,"No se pudo obtener el mensaje");
            // Debug.LogError("Error: " + request.error);
        }
        else
        {
            MsgData response = JsonUtility.FromJson<MsgData>(request.downloadHandler.text);
            if (response.finalizado)
            {
                this.gameObject.GetComponent<TextMeshProUGUI>().text = "¡Felicidades! Has ganado un consejo de emprendimiento: "+ response.datos.mensaje;
            }
            else
            {
                MenuManagerQuiz.inst.mostrarMensaje(0,"No se pudo obtener el mensaje");
                Debug.Log("Authentication failed.");
            }
        }
    }


    void OnDisable()
    {
        EndQuiz.OnActionMsg -= OnActionMsgEmprendimiento;
    }
    void OnEnable()
    {
        EndQuiz.OnActionMsg += OnActionMsgEmprendimiento;
    }

    void OnActionMsgEmprendimiento()
    {
        StartCoroutine(getMsgEmprendimiento());
    }
}

