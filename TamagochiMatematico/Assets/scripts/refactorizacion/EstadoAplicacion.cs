using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
public class EstadoAplicacion : MonoBehaviour
{
    public static EstadoAplicacion estadoAplicacion;
    public static event Action<int, string> OnActionEstadoAppMessage;
    
    private void Awake()
    {
        if (estadoAplicacion == null)
        {
            estadoAplicacion = this;
        }
    }
    IEnumerator estadoAppPeticion()
    {
        const string echoServer = "https://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 5;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        if (result == false && OnActionEstadoAppMessage != null)
        {
            OnActionEstadoAppMessage.Invoke(2, "Sin coneccion a internet");
        }
    }


    public void verificarEstado()
    {
        StartCoroutine(estadoAppPeticion());
    }
}



