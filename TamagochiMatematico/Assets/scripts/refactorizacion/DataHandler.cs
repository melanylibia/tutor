using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DataHandler : MonoBehaviour
{
    public Login.UsuarioLoginData1 usuarioLoginData;
    public static DataHandler inst;
    void Start()
    {
        inst = this;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnEnable()
    {
        Login.OnActionDataHandler += OnActionUsuarioDataHandler;
    }
    private void OnDisable()
    {
        Login.OnActionDataHandler -= OnActionUsuarioDataHandler;
    }
    void OnActionUsuarioDataHandler(Login.UsuarioLoginData1 response)
    {
        usuarioLoginData = response;
    }

}
