
using UnityEngine;
using System.Text.RegularExpressions;
public class BtnRepaso : MonoBehaviour
{
    private string message;
    public string Message
    {
        get { return message; }
        set { message = value; }
    }
    public bool verifyURL(string url)
    {
        string pattern = @"^(https?|ftp):\/\/(-\.)?([^\s\/?\.#]+\.?)+(\/[^\s]*)?$";
        Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
        return regex.IsMatch(url);
    }
    void OnEnable()
    {
        this.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(redireccionarRecomendacion);
    }

    public void redireccionarRecomendacion()
    {
        if (this.message != "")
        {
            bool isURL = verifyURL(this.message);
            if (isURL)
            {
                Application.OpenURL(this.message);
            }
            else
            {
                MenuManagerQuiz.inst.limpiarMsgApoyo();
                MenuManagerQuiz.inst.promptRepaso.SetActive(true);
                MenuManagerQuiz.inst.cambiarMsgApoyo(this.message);
            }
        }
    }
    public void redireccionarRecompensa(string url)
    {
        if (url != "")
        {
            Application.OpenURL(url);
        }
    }
}
