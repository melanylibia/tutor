using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
public class DraggableItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    public Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    // public Image image;
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        // print("OnBeginDrag");

        canvasGroup.blocksRaycasts = false;
        // parentingAfterDrag = transform.parent;
        // transform.SetParent(transform.root);
        // transform.SetAsLastSibling();
        // image.raycastTarget = false;
    }

    public void OnDrag(PointerEventData eventData)
    {

        // print("OnDrag");
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        // transform.position = Input.mousePosition;     
    }

    public void OnEndDrag(PointerEventData eventData)
    {

        canvasGroup.blocksRaycasts = true;
        // print("OnEndDrag");
        // transform.SetParent(parentingAfterDrag);
        // image.raycastTarget = true;
    }
}

