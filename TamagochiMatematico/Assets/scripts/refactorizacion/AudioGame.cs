using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AudioGame : MonoBehaviour
{
    public static AudioGame audioGame;
    AudioSource audioSource;
    private bool isSoundPaused = false;
    public bool IsSoundPaused
    {
        get { return isSoundPaused; }
        set { isSoundPaused = value; }
    }
    public Sprite music_sound_on;
    public Sprite music_sound_off;
    public Image current_music_sound;
    
    public void setupAudio()
    {
        if (audioGame ==null) {
            audioGame = this;
            audioSource = GetComponent<AudioSource>();
            current_music_sound.sprite = music_sound_on;
        }
    }
    public void PausarSonido(bool pausar)
    {
        if (pausar)
        {
            audioSource.Pause();
        }
        else
        {
            audioSource.UnPause();
        }
    }
    public void Pausar()
    {

        if (MainGameController.inst.IsSoundPaused == false)
        {
            current_music_sound.sprite = music_sound_off;
            PausarSonido(true);
            MainGameController.inst.IsSoundPaused = true;
        }
        else
        {
            current_music_sound.sprite = music_sound_on;
            PausarSonido(false);
            MainGameController.inst.IsSoundPaused = false;
        }
    }
    public void Despausar()
    {
        audioSource.UnPause();
        isSoundPaused = false;
    }
}


  