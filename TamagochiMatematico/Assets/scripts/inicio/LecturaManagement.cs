using UnityEngine;
using System;
using System.Xml;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class LecturaManagement : MonoBehaviour
{

    public TextMeshProUGUI mensaje_text;
    public Button Btn_aceptar;
    public LecturaScriptableObject lectura;
    public GameObject menuFinal;
    public GameObject menuInicial;
    public int indicetextoLeccionArray;
    public string[] textoLeccionArray;
    public void startCanvas(int indice)
    {
        menuFinal.SetActive(false);
        menuInicial.SetActive(true);
        mensaje_text.text = textoLeccionArray[indice];
    }
    void Start()
    {
        // indicetextoLeccionArray = 0;
        // // int indice = audioScript.inst.state.nivel;
        // textoLeccionArray = lectura.lecturas[indice].contenido.Split('~');
        // if (indice<lectura.lecturas.Count)
        // {
        //     startCanvas(indicetextoLeccionArray);
        // }
        // else {
        //     menuInicial.SetActive(false);
        //     menuFinal.SetActive(true);
        // }
    }
    public void siguiente()
    {
        if (textoLeccionArray.Length - 1 > indicetextoLeccionArray)
        {
            indicetextoLeccionArray++;
            string[] parrafo = textoLeccionArray[indicetextoLeccionArray].Split('|');
            mensaje_text.text = parrafo[0];
            if (parrafo.Length > 1)
            {
                mensaje_text.text += "\n" + parrafo[1];
            }
        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }
    public void cambiarEscena()
    {
        SceneManager.LoadScene(2);
    }
    public void rehacer()
    {
        // audioScript.inst.state.nivel = 0;
        // audioScript.inst.Save();
        SceneManager.LoadScene(1);
    }
}
