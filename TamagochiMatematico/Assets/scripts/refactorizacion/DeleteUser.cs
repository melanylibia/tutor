using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
public class DeleteUser : MonoBehaviour
{

    public static DeleteUser inst;

    [Serializable]
    public class UsuarioDeletedDatos
    {
        public string id;
        public string estado;

    }

    private string idItem = "";
    public string IdItem
    {
        get { return idItem; }
        set { idItem = value; }
    }
    [Serializable]
    public class UsuarioDeleted
    {
        public bool finalizado;
        public UsuarioDeletedDatos datos;
    }
    private void Start() {
        if(inst == null){
            inst = this;
        }
    }
    public static event Action<int,string> OnActionDeleteUserMessage;
    IEnumerator deleteUser(string url)
    {
        byte[] formData = null;
        UnityWebRequest request =  UnityWebRequest.Put(url, formData);
        request.method = "PATCH";
        request.SetRequestHeader("Authorization", "Bearer " + DataHandler.inst.usuarioLoginData.datos.access_token);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            if (OnActionDeleteUserMessage != null) {
                OnActionDeleteUserMessage.Invoke(0,"error al eliminar usuario");
            }
            Debug.LogError("Error: " + request.error);
        }
        else
        {
            UsuarioDeleted response = JsonUtility.FromJson<UsuarioDeleted>(request.downloadHandler.text);          
            MenuController.inst.activarMenuProfesor();
            MenuController.inst.ocultarMsgEliminar();
        }   
    }


    public void borrarUsuario()
    {
        // string id = this.gameObject.name;
        if (idItem != "")
        {
            string url = "https://tutor-api.hacklab.org.bo/api/usuarios/" + idItem + "/inactivacion";
            StartCoroutine(deleteUser(url));
        }
    }

    public void OnActionMsgEliminar(){
        gameObject.SetActive(true);
    }
}
// curl -X 'PATCH' \
//   'https://tutor-api.hacklab.org.bo/api/usuarios/3/inactivacion' \
//   -H 'accept: */*' \
// {
//   "finalizado": true,
//   "mensaje": "Registro actualizado con éxito.",
//   "datos": {
//     "id": "3",
//     "estado": "INACTIVO"
//   }
// }