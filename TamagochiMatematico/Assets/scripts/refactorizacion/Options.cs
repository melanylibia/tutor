using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Options : MonoBehaviour
{
    PreguntasEtapa0 questions;
    private bool isCorrect = false;
    public bool IsCorrect
    {
        get { return isCorrect; }
        set { isCorrect = value; }
    }
    private Transform posicionInicial = null;
    public Transform PosicionInicial
    {
        get { return posicionInicial; }
        set { posicionInicial = value; }
    }
    void Start()
    {
        //GetComponent<Button>().onClick.AddListener(submit);
        GameObject objWithTag = GameObject.FindWithTag("Quiz");
        if (objWithTag != null)
        {
            questions = objWithTag.GetComponent<PreguntasEtapa0>();
        }
        else
        {
            Debug.Log(" Not founded");
        }
    }
    public void submit()
    {
        questions.submitAnswerQuiz(IsCorrect);
    }
}
