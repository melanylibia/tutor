using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public class CreateUsuario : MonoBehaviour
{
    [Serializable]
    public class UsuarioNuevoData
    {
        public string idLeccion;
        public string usuario;
        public string contrasena;
        public string[] roles;
    }
    public static event Action<int,string> OnActionCreateNewUserMessage;
    IEnumerator crearUsuarioRequest(string user, string passwd)
    {
        string url = "https://tutor-api.hacklab.org.bo/api/usuarios";
        UsuarioNuevoData requestData = new UsuarioNuevoData
        {
            idLeccion = "1",
            usuario = user,
            contrasena = passwd,
            roles = new string[] { "3" }
        };

        string jsonData = JsonUtility.ToJson(requestData);
        UnityWebRequest request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(jsonData);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        if(DataHandler.inst)
        {
            request.SetRequestHeader("Authorization", "Bearer " + DataHandler.inst.usuarioLoginData.datos.access_token);
        }

        // Send the request
        yield return request.SendWebRequest();

        // Check for errors
        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error: " + request.error);
            if (OnActionCreateNewUserMessage != null) {
                OnActionCreateNewUserMessage.Invoke(0,"error en la creacion de usuario");
            }
        }
        else
        {
            if (OnActionCreateNewUserMessage != null) {
                OnActionCreateNewUserMessage.Invoke(1,"creacion de usuario exitosa");
            }
            Debug.Log("Usuario creado");
        }
    }
    void OnEnable()
    {
        MenuController.OnActionCrearUsuario += OnActionEventHandlerCrearUsuario;
    }

    void OnDisable()
    {
        MenuController.OnActionCrearUsuario -= OnActionEventHandlerCrearUsuario;
    }

    void OnActionEventHandlerCrearUsuario(string user, string passwd)
    {
        StartCoroutine(crearUsuarioRequest(user, passwd));
    }
}
