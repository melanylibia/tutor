using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Question
{
    public string text;
    public string answer;
    public string apoyo;

    public Question(string text, string answer, string apoyo)
    {
        this.text = text;
        this.answer = answer;
        this.apoyo = apoyo;
    }
}