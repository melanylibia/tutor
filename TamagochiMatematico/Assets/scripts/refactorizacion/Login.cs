using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{

    [System.Serializable]
    public class UsuarioLoginData1
    {
        public bool finalizado;
        public UsuarioData datos;

        [System.Serializable]
        public class UsuarioData
        {
            public string access_token;
            public string usuario;
            public string idRol;
            public bool pruebaRealizada;
            public string rol;
            public string idLeccion;
            public string leccion;
            public string estado;
            public string id;
        }
    }
    public static event Action<UsuarioLoginData1> OnActionDataHandler;
    public static event Action<int,string> OnActionLoginMessage;
    IEnumerator loginRequest(string user, string passwd)
    {
        string url = "https://tutor-api.hacklab.org.bo/api/auth";
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(passwd);
        string base64Passwd = Convert.ToBase64String(bytes);
        string jsonData = "{\"usuario\":\"" + user + "\",\"contrasena\":\"" + base64Passwd + "\"}";
        UnityWebRequest request = UnityWebRequest.PostWwwForm(url, jsonData);
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("accept", "*/*");

        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(jsonData);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("Error: " + request.error);
            if (OnActionLoginMessage != null) {
                OnActionLoginMessage.Invoke(0,"error con las credenciales");
            }
        }
        else
        {
            UsuarioLoginData1 response = JsonUtility.FromJson<UsuarioLoginData1>(request.downloadHandler.text);
            if (response.finalizado)
            {
                OnActionDataHandler.Invoke(response);
                if (response.datos.rol == "ADMINISTRADOR" && MenuController.inst)
                {
                    OnActionLoginMessage.Invoke(1, "registro exitoso");
                    MenuController.inst.menuAdmin.SetActive(true);
                    MenuController.inst.login.SetActive(false);

                }
                else
                {
                    OnActionLoginMessage.Invoke(1, "registro exitoso");
                    // £££ aumentar una coroutina
                    if(!response.datos.pruebaRealizada){
                        MenuController.inst.activarTestInicial();
                        TestInicial.testInicial.IdUsuario = response.datos.id;
                    }
                    else{
                        SceneManager.LoadScene("juego");
                    }
                }
            }
            else
            {
                Debug.Log("Authentication failed.");
            }
        }
    }
    void OnDisable()
    {
        MenuController.OnActionLogin -= OnActionEventHandlerLogin;
    }
    void OnEnable()
    {
        MenuController.OnActionLogin += OnActionEventHandlerLogin;
    }

    void OnActionEventHandlerLogin(string usuario, string passwd)
    {
        StartCoroutine(loginRequest(usuario, passwd));
    }
}
