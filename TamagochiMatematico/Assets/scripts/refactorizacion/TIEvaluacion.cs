using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
public class TIEvaluacion : MonoBehaviour
{
    public static event Action<int, string> OnActionTIEvaluationMessage;
    IEnumerator evaluacionInicialRequest(string url, TestInicial.TestRespuestaData data)
    {
        string jsonData = JsonUtility.ToJson(data);
        byte[] formData = System.Text.Encoding.UTF8.GetBytes(jsonData);
        UnityWebRequest request = UnityWebRequest.Put(url, formData);
        request.method = "PATCH";
        request.SetRequestHeader("Authorization", "Bearer " + DataHandler.inst.usuarioLoginData.datos.access_token);
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            if (OnActionTIEvaluationMessage != null)
            {
                OnActionTIEvaluationMessage.Invoke(0, "No fue posible enviar las respuestas del cuestionario. Inténtalo más tarde.");
            }
            Debug.LogError("Error: " + request.error);
        }
        else
        {
            Debug.Log("PATCH request successful!");
            // Debug.Log(request.downloadHandler.text);
            if (OnActionTIEvaluationMessage != null)
            {
                OnActionTIEvaluationMessage.Invoke(1, "¡Excelente trabajo! ¿Listo para empezar la lección?");
                SceneManager.LoadScene("juego");   
            }
        }
    }

    //OnActionSubmitTI

    void OnEnable()
    {
        TestInicial.OnActionSubmitTI += actualizarEstadoUsuario;
    }

    void OnDisable()
    {
        TestInicial.OnActionSubmitTI -= actualizarEstadoUsuario;
    }

    public void actualizarEstadoUsuario(TestInicial.TestRespuestaData user, string id)
    {
        if (id != "")
        {
            string url = "https://tutor-api.hacklab.org.bo/api/usuarios/" + id;
            StartCoroutine(evaluacionInicialRequest(url, user));
        }
    }
}
