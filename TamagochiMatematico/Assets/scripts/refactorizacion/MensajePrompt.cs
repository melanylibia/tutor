using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class MensajePrompt : MonoBehaviour
{
    public TMP_Text txt_mensaje;
    public Image img;
    private  Color RED = Color.red;
    private  Color GREEN = Color.green;
    private  Color YELLOW = Color.blue;

    public void warningMessage(string text)
    {
        StartCoroutine(cambiarColor(RED,text));
    }
    public void successMessage(string text)
    {
        StartCoroutine(cambiarColor(GREEN, text));
    }
    public void infoMessage(string text, float time=4.5f)
    {
        StartCoroutine(cambiarColor(YELLOW, text, time));
    }
       
    IEnumerator cambiarColor(Color color, string text, float time=4.5f)
    {
        img.color = color;
        txt_mensaje.text = text;
        yield return new WaitForSeconds(time);
        this.gameObject.SetActive(false);
    }
}
