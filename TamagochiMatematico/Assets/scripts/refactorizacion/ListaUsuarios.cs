using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.Networking;
public class ListaUsuarios : MonoBehaviour
{
    
    [Serializable]
public class Lesson
{
    public string titulo;
}

[Serializable]
public class Nota
{
    public int intentos;
    public Lesson leccion;
}

[Serializable]
public class Persona
{
    public string nombres;
    public string primerApellido;
    public string segundoApellido;
}

[Serializable]
public class Fila
{
    public string estado;
    public string usuario;
    public string id;
    public Nota[] notas;
    public Persona persona;
}

[Serializable]
public class Datos
{
    public int total;
    public Fila[] filas;
}

[Serializable]
public class ApiResponse
{
    public bool finalizado;
    public string mensaje;
    public Datos datos;
}
    public GameObject userDataPrefab;
    public Transform spawnPoint;
    public Transform containerPoint;
    public GameObject detalleUser;
    public IEnumerator getUserList()
{
    // API endpoint URL

    string url = "https://tutor-api.hacklab.org.bo/api/usuarios?limite=50&pagina=1";

    // Create the request
    UnityWebRequest request = UnityWebRequest.Get(url);
    request.SetRequestHeader("Authorization", "Bearer " + DataHandler.inst.usuarioLoginData.datos.access_token);

    // Send the request
    yield return request.SendWebRequest();

    // Check for errors
    if (request.result != UnityWebRequest.Result.Success)
    {
        Debug.LogError("Error: " + request.error);
    }
    else
    {
        ApiResponse response = JsonUtility.FromJson<ApiResponse>(request.downloadHandler.text);
        int i = 0;
        spawnPoint.localScale = Vector3.one;
        foreach (Transform child in containerPoint)
        {
            Destroy(child.gameObject);
        }
        foreach (var fila in response.datos.filas)
        {
            //fila.usuario + "\n";
            GameObject newUserDataPrefab = Instantiate(userDataPrefab, spawnPoint.position + Vector3.up * 100 * i, Quaternion.identity);
            Transform userText = newUserDataPrefab.transform.Find("dato-usuario");
            TextMeshProUGUI userTextComponent = userText.GetComponent<TextMeshProUGUI>();
            userTextComponent.text = fila.usuario;
            newUserDataPrefab.transform.SetParent(containerPoint);
            newUserDataPrefab.transform.localScale = Vector3.one;
            newUserDataPrefab.transform.name = fila.id;
            i++;
        }

    }
}

    void OnEnable()
    {
        MenuController.OnActionListaUsuarios += OnActionEventHandlerListaUsuarios;
        OnActionEventHandlerListaUsuarios();
        detalleUser.SetActive(false);
    }

    void OnDisable()
    {
        MenuController.OnActionListaUsuarios -= OnActionEventHandlerListaUsuarios;
    }

    void OnActionEventHandlerListaUsuarios()
    {
        StartCoroutine(getUserList());
    }
}
