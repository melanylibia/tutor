using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInfo : MonoBehaviour
{
    public void userDetail()
    {
        MenuController.inst.usuarioDetalle.gameObject.SetActive(true);
        MenuController.inst.DetalleUsuario(this.gameObject.name);
    }
    public void openPromptEliminar(){
        MenuController.inst.mostrarMsgEliminar(this.gameObject.name);
    }
}
