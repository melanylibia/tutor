using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Player : MonoBehaviour
{
    public static Player inst;
    public Animator animator;
    private int tasaDeFelicidad = 5;
    public int TasaDeFelicdad
    {
        get { return tasaDeFelicidad; }
        set { tasaDeFelicidad = value; }
    }
    private string estado;
    public string Estado
    {
        get { return estado; }
        set { estado = value; }
    }
    private bool continuar = true;
    public bool Continuar
    {
        get { return continuar; }
        set { continuar = value; }
    }
    private bool pruebaRealizada = false;
    public bool PruebaRealizada
    {
        get { return pruebaRealizada; }
        set { pruebaRealizada = value; }
    }
    private int felicidad = 100;
    public int Felicidad
    {
        get { return felicidad; }
        set { felicidad = value; }
    }
    private string _userToken;
    public string UserToken
    {
        get { return _userToken; }
        set { _userToken = value; }
    }
    private string _username;
    public string Username
    {
        get { return _username; }
        set { _username = value; }
    }
    private string nombreLeccionActual;
    public string NombreLeccionActual
    {
        get { return nombreLeccionActual; }
        set { nombreLeccionActual = value; }
    }
    private int nivelActual;      
    public int NivelActual
    {
        get { return nivelActual; }
        set { nivelActual = value; }
    }
    public static event Action OnActionQuiz;

    void OnDisable()
    {
        RespuestasQuiz.OnActionUpdatedUserHandler -= onActionUpdated;
        PreguntasEtapa1.OnActionPlayerCorrectAnswer -= playerCorrectAnswer;
        PreguntasEtapa0.OnActionPlayerCorrectAnswer -= playerCorrectAnswer;
    }
    void OnEnable()
    {
        RespuestasQuiz.OnActionUpdatedUserHandler += onActionUpdated;
        PreguntasEtapa1.OnActionPlayerCorrectAnswer += playerCorrectAnswer;
        PreguntasEtapa0.OnActionPlayerCorrectAnswer += playerCorrectAnswer;
    }

    public void onActionUpdated(int nivel,string leccion)
    {
        nivelActual = nivel;
        nombreLeccionActual = leccion;
    }
    void Awake() {

        inst = this;
        clearVariables();
        if (animator == null)
            animator = GetComponent<Animator>();
        if(DataHandler.inst)
        {
            _username = DataHandler.inst.usuarioLoginData.datos.usuario;
            _userToken = DataHandler.inst.usuarioLoginData.datos.access_token;
            pruebaRealizada = DataHandler.inst.usuarioLoginData.datos.pruebaRealizada;
            nivelActual = int.Parse(DataHandler.inst.usuarioLoginData.datos.idLeccion);
            nombreLeccionActual = DataHandler.inst.usuarioLoginData.datos.leccion;
            estado = DataHandler.inst.usuarioLoginData.datos.estado;
        }

        if (OnActionQuiz != null)
        {
            OnActionQuiz.Invoke();
        }
    }
    public void clearVariables() {
        _username = "";
        _userToken = "";
        nombreLeccionActual = "";
        nivelActual = 0;
    }
    void Salir()
    {
        clearVariables();
    }
    
    void Morir()
    {
        nivelActual = 0;
    }
    void playerCorrectAnswer()
    {
        animator.SetTrigger("correct");   
    }
}
