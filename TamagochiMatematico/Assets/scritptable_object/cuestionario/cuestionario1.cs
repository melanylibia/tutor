using UnityEngine;
using System.Collections.Generic;
using Unity.VisualScripting;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CuestionarioScriptableObject", order = 1)]

public class CuestionarioScriptableObject : ScriptableObject
{

    [SerializeField]
    public List<Question> questions;

}