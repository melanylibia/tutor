using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
public class ProfesoraReporte : MonoBehaviour
{
    [System.Serializable]
    public class UsuariosReporteData
    {
        public bool finalizado;
        public ReporteData datos;

        public string mensaje;

        [System.Serializable]
        public class ReporteData
        {
            public string reporte;
        }
    }
    public static event Action<int, string> OnActionReporteEstudiantes;
    IEnumerator descargarReporteEstudiantesRequest()
    {
        string url = "https://tutor-api.hacklab.org.bo/api/usuarios/reportes";
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("Authorization", "Bearer " + DataHandler.inst.usuarioLoginData.datos.access_token);
        yield return request.SendWebRequest();
        UsuariosReporteData response = JsonUtility.FromJson<UsuariosReporteData>(request.downloadHandler.text);

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error: " + request.error);
            if (OnActionReporteEstudiantes != null)
            {
                OnActionReporteEstudiantes.Invoke(0, response.mensaje);
            }
        }
        else
        {
            string urlReporte = "https://proyectos-lib.casillero.laotrared.net/" + response.datos.reporte;
            Application.OpenURL(urlReporte);
            if (OnActionReporteEstudiantes != null)
            {
                OnActionReporteEstudiantes.Invoke(1, "reporte de estudiantes obtenidos con exito");
            }
        }
    }
    public void descargarReporteEstudiantes()
    {
        StartCoroutine(descargarReporteEstudiantesRequest());
    }

    public static event Action<int, string> OnActionReporteTiempo;
    //  Tiempo
    IEnumerator descargarReporteTiempoRequest()
    {
        string url = "https://tutor-api.hacklab.org.bo/api/notas/reportesTiempo";
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("Authorization", "Bearer " + DataHandler.inst.usuarioLoginData.datos.access_token);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error: " + request.error);
            if (OnActionReporteTiempo != null)
            {
                OnActionReporteTiempo.Invoke(0, "no se pudo obtener record");
            }
        }
        else
        {
            UsuariosReporteData response = JsonUtility.FromJson<UsuariosReporteData>(request.downloadHandler.text);
            string urlReporte = "https://proyectos-lib.casillero.laotrared.net/" + response.datos.reporte;
            Application.OpenURL(urlReporte);

            if (OnActionReporteTiempo != null)
            {
                OnActionReporteTiempo.Invoke(1, "reporte de tiempo promedio obtenido con exito");
            }
        }
    }
    public void descargarReporteTiempo()
    {
        StartCoroutine(descargarReporteTiempoRequest());
    }
    //  Intentos
    public static event Action<int, string> OnActionReporteIntentos;
    IEnumerator descargarReporteIntentosRequest()
    {
        string url = "https://tutor-api.hacklab.org.bo/api/notas/reportesIntentos";
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("Authorization", "Bearer " + DataHandler.inst.usuarioLoginData.datos.access_token);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error: " + request.error);
              if (OnActionReporteIntentos != null)
            {
                OnActionReporteIntentos.Invoke(0, "no se pudo obtener record");
            }
        }
        else
        {
            UsuariosReporteData response = JsonUtility.FromJson<UsuariosReporteData>(request.downloadHandler.text);
            string urlReporte = "https://proyectos-lib.casillero.laotrared.net/" + response.datos.reporte;
            Application.OpenURL(urlReporte);
            if (OnActionReporteIntentos != null)
            {
                OnActionReporteIntentos.Invoke(1, "reporte de intentos promedio obtenido con exito");
            }
        }
    }
    public void descargarReporteIntentos()
    {
        StartCoroutine(descargarReporteIntentosRequest());
    }
}
