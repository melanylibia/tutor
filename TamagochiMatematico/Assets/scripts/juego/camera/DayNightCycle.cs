using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    public Color[] targetColors; // Array to hold your 4 colors
    public float transitionDuration = 2f; // Duration of each transition in seconds

    private Camera mainCamera; // Reference to the main camera

    private void Start()
    {
        mainCamera = Camera.main; // Get the main camera
        StartCoroutine(ContinuousTransition());
    }

    // Coroutine for continuous color transitions
    private IEnumerator ContinuousTransition()
    {
        while (true)
        {
            Color startColor = mainCamera.backgroundColor; // Get the current background color
            Color endColor = targetColors[Random.Range(0, targetColors.Length)]; // Choose a random target color

            float elapsedTime = 0f;

            while (elapsedTime < transitionDuration)
            {
                elapsedTime += Time.deltaTime;
                float t = Mathf.Clamp01(elapsedTime / transitionDuration);
                mainCamera.backgroundColor = Color.Lerp(startColor, endColor, t);
                yield return null;
            }

            mainCamera.backgroundColor = endColor; // Ensure we end up with the exact target color

            yield return null; // Wait for a frame before starting the next transition
        }
    }

    // public Color nightColor;
    // public Color morningColor;
    // public float transitionDuration = 180f; // 3 minutes
    
    // private Camera mainCamera;
    // private float transitionTimer = 0f;
    // public bool isNigth= false;
    // // public GameObject btnAlimentar;

    // private void Start()
    // {
    //     // mainCamera = Camera.main;
    //     // mainCamera.backgroundColor = nightColor;
    // }

    // private void Update()
    // {
    //     // float t = Mathf.Clamp01(transitionTimer / transitionDuration);
    //     // if (isNigth)
    //     // {
    //     //     // btnAlimentar.SetActive(false);
    //     //     mainCamera.backgroundColor = Color.Lerp(morningColor, nightColor, t);
    //     //     transitionTimer += Time.deltaTime;
    //     // }
    //     // else
    //     // {
    //     //     // btnAlimentar.SetActive(true);
    //     //     transitionTimer += (Time.deltaTime / 3);
    //     //     mainCamera.backgroundColor = Color.Lerp(nightColor, morningColor, t);
    //     // }
    //     // if (transitionTimer >= transitionDuration)
    //     // {
    //     //     transitionTimer = 0f;
    //     //     isNigth = !isNigth;
    //     // }

    // }
}
