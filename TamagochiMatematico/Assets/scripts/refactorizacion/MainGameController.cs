using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameController : MonoBehaviour
{
    public static MainGameController inst;
    private bool levelPassed = false;
    public bool LevelPassed
    {
        get { return levelPassed; }
        set { levelPassed = value; }
    }
    private bool isSoundPaused = false;
    public bool IsSoundPaused
    {
        get { return isSoundPaused; }
        set { isSoundPaused = value; }
    }

    /// <summary>
    ///  PlMain controller has part of player
    /// </summary>
    void Awake()
    {
        if (inst == null)
        {
            inst = this;
            this.gameObject.GetComponent<AudioGame>().setupAudio();
            DontDestroyOnLoad(this.gameObject);
        }
        else if (inst != this)
        {

            Destroy(this.gameObject);
        }
    }
    

}
