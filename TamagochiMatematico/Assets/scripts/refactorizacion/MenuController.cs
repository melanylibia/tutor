using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;
public class MenuController : MonoBehaviour
{

    public static MenuController inst;
    public UsuarioDetalle usuarioDetalle;
    public TMP_InputField usuario;
    public TMP_InputField password;
    public TMP_InputField new_usuario;
    public TMP_InputField new_password;
    public TMP_InputField repeat_password;
    public GameObject menuAdmin;
    public GameObject menuCrearUser;
    public GameObject login;
    public GameObject menuLista;
    public GameObject message;

    public GameObject pantallaTInicial;
    public GameObject PromptEliminar;
    //
    void Start()
    {
        if (inst == null)
        {
            EstadoAplicacion.estadoAplicacion.verificarEstado();
            inst = this;
            menuAdmin.SetActive(false);
            PromptEliminar.SetActive(false);
            message.SetActive(false);
        }
    }
    public void mostrarMsgEliminar(string id){
        PromptEliminar.SetActive(true);
        PromptEliminar.GetComponent<DeleteUser>().IdItem = id;
    }
    public void ocultarMsgEliminar(){
        PromptEliminar.SetActive(false);
    }
    
    public static event Action<string, string> OnActionCrearUsuario;
    public static event Action OnActionListaUsuarios;
    public static event Action<string, string> OnActionLogin;
    public void btnLogin()
    {
        if (OnActionLogin != null)
        {
            OnActionLogin.Invoke(usuario.text, password.text);
        }
    }
    public void clearFields()
    {
        usuario.text = "";
        password.text = "";
        new_usuario.text = "";
        new_password.text = "";
        repeat_password.text = "";
    }

    public void Crear()
    {
        if (OnActionCrearUsuario != null)
        {
            if (new_password.text == repeat_password.text) OnActionCrearUsuario.Invoke(new_usuario.text, new_password.text);
            else
            {
                OnActionLoginMessageHandler(0, "Los campos de contraseña no son iguales");
            }
        }
    }
    public void ListaUsuarios()
    {
        if (OnActionListaUsuarios != null)
        {
            OnActionListaUsuarios.Invoke();
        }
    }
    public void DetalleUsuario(string id)
    {
        if (usuarioDetalle)
        {
            usuarioDetalle.mostartDetalleUsuario(id);
        }
    }
    void OnDisable()
    {
        Login.OnActionLoginMessage -= OnActionLoginMessageHandler;
        CreateUsuario.OnActionCreateNewUserMessage -= OnActionCreatedUserMessage;
        ProfesoraReporte.OnActionReporteEstudiantes -=  OnActionPromp;
        ProfesoraReporte.OnActionReporteTiempo -=  OnActionPromp;
        ProfesoraReporte.OnActionReporteIntentos -=  OnActionPromp;
        UsuarioDetalle.OnActionUDetailMessage -=  OnActionPromp;
        TIEvaluacion.OnActionTIEvaluationMessage -=  OnActionPromp;
        EstadoAplicacion.OnActionEstadoAppMessage-= OnActionPromp;
        DeleteUser.OnActionDeleteUserMessage -=OnActionPromp;
    }
    void OnEnable()
    {
        Login.OnActionLoginMessage += OnActionLoginMessageHandler;
        CreateUsuario.OnActionCreateNewUserMessage += OnActionCreatedUserMessage;
        ProfesoraReporte.OnActionReporteEstudiantes +=  OnActionPromp;
        ProfesoraReporte.OnActionReporteTiempo +=  OnActionPromp;
        ProfesoraReporte.OnActionReporteIntentos +=  OnActionPromp;
        UsuarioDetalle.OnActionUDetailMessage +=  OnActionPromp;
        TIEvaluacion.OnActionTIEvaluationMessage += OnActionPromp;
        EstadoAplicacion.OnActionEstadoAppMessage += OnActionPromp;
        DeleteUser.OnActionDeleteUserMessage += OnActionPromp;
    }
    public void OnActionPromp(int code, string msg){
        OnActionLoginMessageHandler(code, msg);
    }
    public void OnActionCreatedUserMessage(int code, string msg)
    {
        OnActionLoginMessageHandler(code, msg);
        clearFields();
        if (code != 0)
        {
            menuAdmin.SetActive(true);
            login.SetActive(false);
            menuLista.SetActive(false);
            menuCrearUser.SetActive(false);
        }
    }
    void OnActionLoginMessageHandler(int code, string message)
    {
        switch (code)
        {
            case 0:
                this.message.SetActive(true);
                this.message.GetComponentInChildren<MensajePrompt>().warningMessage(message);
                break;
            case 1:
                this.message.SetActive(true);
                this.message.GetComponentInChildren<MensajePrompt>().successMessage(message);
                break;
            case 2:
                this.message.SetActive(true);
                this.message.GetComponentInChildren<MensajePrompt>().infoMessage(message,5.0f);
                break;
            default:
                break;
        }
    }

    public void activarMenuProfesor()
    {
        login.SetActive(false);
        menuLista.SetActive(false);
        pantallaTInicial.SetActive(false);
        menuAdmin.SetActive(true);
    }
    public void activarTestInicial()
    {
        login.SetActive(false);
        menuLista.SetActive(false);
        menuAdmin.SetActive(false);
        pantallaTInicial.SetActive(true);
    }
}
