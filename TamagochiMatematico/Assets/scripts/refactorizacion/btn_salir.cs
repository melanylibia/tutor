using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class btn_salir : MonoBehaviour
{

    void Start()
    {
        
    }

    // Update is called once per frame
    public void salir()
    {
        AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        activity.Call<bool>("moveTaskToBack", true);
    }
}