﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using System;
public class UsuarioDetalle : MonoBehaviour
{
    public TMP_Text detalleUsuario;
    [Serializable]
    public class Leccion
    {
        public string titulo;
    }

    [Serializable]
    public class UsuarioRecordPersona
    {
        public string nombres;
        public string primerApellido;
        public string segundoApellido;
    }

    [Serializable]
    public class UsuarioRecordNota
    {
        public int intentos;
        public int tiempoLeccion;
        public Leccion leccion;
    }

    [Serializable]
    public class UsuarioRecordDatos
    {
        public string usuario;
        public UsuarioRecordNota[] notas;
        public UsuarioRecordPersona persona;
    }

    [Serializable]
    public class UsuarioRecord
    {
        public bool finalizado;
        public UsuarioRecordDatos datos;
    }
    public string formatTimePlayed(int t)
    {
        int min = (int)t / 60;
        string minutes = min.ToString();
        int sec = (int)(t % 60);
        string seconds = sec.ToString();
        return minutes + ":" + seconds;
    }
    public UsuarioRecord response;
    public static event Action<int,string> OnActionUDetailMessage;
    IEnumerator getUserDetail(string url, string id)
    {
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("Authorization", "Bearer " + DataHandler.inst.usuarioLoginData.datos.access_token);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error: " + request.error);
            if(OnActionUDetailMessage!=null){
                OnActionUDetailMessage.Invoke(0, "No fue posible enviar las respuestas del cuestionario. Inténtalo más tarde.");
            }
        }
        else
        {
            string textoDetalle = "";
            response = JsonUtility.FromJson<UsuarioRecord>(request.downloadHandler.text);
            if (response.finalizado)
            {
                string previewData = "Sin iniciar";
                if (response.datos.notas.Length != 0)
                {
                    for (int i = 0; i < response.datos.notas.Length; i++)
                    {
                        textoDetalle = textoDetalle + "Leccion: " + response.datos.notas[i].leccion.titulo + "\n";
                        textoDetalle = textoDetalle + "Intentos: " + response.datos.notas[i].intentos + "    " + "tiempo: " + formatTimePlayed(response.datos.notas[i].tiempoLeccion) + "\n";/// XXXXXXXX                        
                    }
                }
                else
                {
                    textoDetalle = "No inicio ninguna leccion";
                }
                detalleUsuario.text = textoDetalle;
            }
            else
            {
                Debug.Log("Authentication failed.");
            }
        }
    }


    public void mostartDetalleUsuario(string id)
    {
        if (id != "")
        {
            string url = "https://tutor-api.hacklab.org.bo/api/usuarios/" + id + "/record";
            StartCoroutine(getUserDetail(url, id));
        }
    }
}
