
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
public class EndQuiz : MonoBehaviour
{
    List<Button> apoyoButtonList;
    public GameObject repaso;
    public GameObject objMessage;
    void Awake()
    {
        apoyoButtonList = new List<Button>();
        foreach (Transform child in repaso.transform.GetChild(1).gameObject.transform)
        {
            apoyoButtonList.Add(child.GetComponent<Button>());
            child.gameObject.SetActive(false);       
        }
    }
    void clearRepaso()
    {
        foreach (Button btn in apoyoButtonList)
        {
            btn.gameObject.SetActive(false);
            btn.gameObject.name = "BtnRepaso";
        }
    }
    void Enable()
    {
        clearRepaso();
    }
    public void setEndQuiz(List<string> stringList)
    {
        clearRepaso();
        int index = 0;
        foreach (string str in stringList)
        {
            apoyoButtonList[index].gameObject.SetActive(true);
            apoyoButtonList[index].gameObject.name = "btn-Repaso";
            apoyoButtonList[index].gameObject.GetComponent<BtnRepaso>().Message=str;
            apoyoButtonList[index].transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "P."+ (index+1).ToString();
            index++;
        }
    }
    public void setMessage() {
        mostrarMsgEmprendimiento();
        //objMessage.GetComponentInChildren<TextMeshProUGUI>().text = "Felicidades! Has completado el quiz.";
    }
    public void PlayAgain() {
        repaso.SetActive(true);
        objMessage.SetActive(false);
    }

    public void NextLesson() {
        repaso.SetActive(false);
        objMessage.SetActive(true);
    }
    public static event Action OnActionMsg;
    public void mostrarMsgEmprendimiento()
    {
        OnActionMsg.Invoke();
    }
}
