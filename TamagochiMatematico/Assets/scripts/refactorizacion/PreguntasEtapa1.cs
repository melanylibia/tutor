﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
public class PreguntasEtapa1 : MonoBehaviour
{
    public bool hasInteres2 = false;
    public TMP_InputField capital_i;
    public TMP_InputField interes;
    public TMP_InputField interes_2;
    public GameObject btn_info;
    public TMP_InputField tiempo;
    public TextMeshProUGUI txt_pregunta;
    public TextMeshProUGUI txt_respuesta;
    public int index = 0;
    public Button BTNAceptar;
    public bool onEnable = true;
    private List<string> repasoList;
    public int nroCorrectas = 0;
    public GameObject time;
    public GameObject promp_info;
    List<int> nivelesInteres2;
    public GameObject screenEndQuiz;

    public void promptInfo(string texto)
    {
        promp_info.transform.GetChild(2).gameObject.GetComponent<TextMeshProUGUI>().text = texto.Replace(".", "\n");
        promp_info.transform.GetChild(1).gameObject.SetActive(false);
    }

    void Setup()
    {
        time.SetActive(true);

        List<int> nivelesInteres2 = new List<int>();
        nivelesInteres2.Add(5);
        nivelesInteres2.Add(7);
        if (BTNAceptar)
        {
            BTNAceptar.onClick.AddListener(enviarRespuesta);
        }
        BTNAceptar.gameObject.SetActive(true);
        onEnable = true;
        nroCorrectas = 0;
        txt_respuesta.text = "";
        repasoList = new List<string>();
        if (Player.inst && nivelesInteres2.Contains(Player.inst.NivelActual))
        {
            interes_2.gameObject.SetActive(true);
            btn_info.gameObject.SetActive(true);
            interes_2.text = "";
        }
        else
        {
            interes_2.gameObject.SetActive(false);
            btn_info.gameObject.SetActive(false);

        }
        screenEndQuiz.SetActive(false);
        interes.transform.parent.gameObject.SetActive(true);
    }
    void Start()
    {
        Setup();
    }
    void manejoActivacion(bool sw)
    {
        BTNAceptar.gameObject.transform.parent.gameObject.SetActive(sw);
        txt_pregunta.gameObject.transform.parent.gameObject.SetActive(sw);
    }

    IEnumerator messageRespuesta(string respuestaUsuario)
    {
        time.GetComponent<TimerPlayer>().pausarTimer();
        manejoActivacion(false);
        txt_respuesta.text = "Respuesta correcta: " + data.datos[index].respuestas[0].texto + "\n" + "Tu respuesta: " + respuestaUsuario;
        yield return new WaitForSeconds(5);
        manejoActivacion(true);
        txt_respuesta.text = "";
        time.GetComponent<TimerPlayer>().pausarTimer();
    }
    void OnEnable()
    {
        index = 0;
        Setup();
    }

    void clearInputFields()
    {
        capital_i.text = "";
        interes.text = "";
        if (hasInteres2)
        {
            interes_2.text = "";
        }
        tiempo.text = "";
    }
    public void EndQuizUI()
    {
        time.GetComponent<TimerPlayer>().pausarTimer();
        time.SetActive(false);
        screenEndQuiz.SetActive(true);
        if (repasoList.Count != 0)
        {
            screenEndQuiz.GetComponent<EndQuiz>().PlayAgain();
            screenEndQuiz.GetComponent<EndQuiz>().setEndQuiz(repasoList);
        }
        else
        {
            screenEndQuiz.GetComponent<EndQuiz>().NextLesson();
            screenEndQuiz.GetComponent<EndQuiz>().setMessage();
        }
        clearScreen();
    }
    public void clearScreen()
    {
        txt_pregunta.text = "";
        interes.text = "";
        capital_i.text = "";
        tiempo.text = "";
        if (interes_2)
        {
            interes_2.text = "";
            interes_2.gameObject.SetActive(false);
            btn_info.gameObject.SetActive(false);
        }
        interes.transform.parent.gameObject.SetActive(false);
    }
    public static event Action OnActionPlayerCorrectAnswer;
    void enviarRespuesta()
    {
        if (tiempo.text == "" || capital_i.text == "" || interes.text == "" || (interes_2.gameObject.activeSelf == true && interes_2.text == "")) return;

        if (index >= data.datos.Count - 1)
        {
            onEnable = false;
        }
        string respuestaUsuario = generarRespuesta().ToString();
        if (data.datos[index].respuestas[0].texto == respuestaUsuario)
        {
            nroCorrectas++;
            OnActionPlayerCorrectAnswer.Invoke();
        }
        else
        {
            repasoList.Add(System.DateTime.Now.ToString() + UnityEngine.Random.Range(1, 100).ToString());
        }
        if (!onEnable)
        {
            if (TimerPlayer.inst.TotalTime <= 0.0f) TimerPlayer.inst.TotalTime = 1.0f;
            RespuestasQuiz.inst.endQuiz(nroCorrectas.ToString(), ((int)TimerPlayer.inst.TotalTime).ToString());
            if (Player.inst.Estado != "COMPLETED") { EndQuizUI(); }
        }
        else
        {
            StartCoroutine(messageRespuesta(respuestaUsuario));
            clearInputFields();
            index++;
            establecerPreguntaUI();
        }
    }
    void establecerPreguntaUI()
    {
        txt_pregunta.text = data.datos[index].texto;
    }
    MenuManagerQuiz.ResponseData data;
    public void submitAnswer(MenuManagerQuiz.ResponseData newData)
    {
        this.data = newData;
        establecerPreguntaUI();
    }

    public int generarRespuesta()
    {
        switch (Player.inst.NivelActual)
        {
            case 4:
                return (int)(float.Parse(capital_i.text) * (1.0 + (float.Parse(interes.text) * float.Parse(tiempo.text))));
            case 5:
                return (int)(float.Parse(capital_i.text) * (1.0 + ((float.Parse(interes.text) / float.Parse(interes_2.text)) * float.Parse(tiempo.text))));
            case 6:
                return (int)(float.Parse(capital_i.text) * float.Parse(interes.text) * float.Parse(tiempo.text));
            case 7:
                return (int)(float.Parse(capital_i.text) * (float.Parse(interes.text) / float.Parse(interes_2.text)) * float.Parse(tiempo.text));
            default:
                return 0;
        }
    }
}