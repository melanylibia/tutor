using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
public class MenuManagerQuiz : MonoBehaviour
{
    public static MenuManagerQuiz inst;
    public ResponseData data;
    public GameObject timerObject;
    public GameObject nivelInfo;
    public GameObject escena1;
    public GameObject escena2;
    public GameObject escena3;
    public GameObject map;
    public GameObject message;
    public GameObject endQuiz;

    public GameObject promptRepaso;

    public void limpiarMsgApoyo(){
        promptRepaso.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "";
    }
    public void cambiarMsgApoyo(string msg){
        promptRepaso.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = msg;
    }
    public void closeExit()
    {
        SceneManager.LoadScene("login");
    }
    [System.Serializable]
    public class Respuesta
    {
        public string id;
        public string texto;
        public bool esCorrecta;
    }
    public int limitQuestions = 5;
    [System.Serializable]
    public class Dato
    {
        public string estado;
        public string id;
        public string texto;
        public string apoyo;
        public string idLeccion;
        public bool esImagen;
        public List<Respuesta> respuestas;
    }

    [System.Serializable]
    public class ResponseData
    {
        public bool finalizado;
        public string mensaje;
        public List<Dato> datos;
    }

    void Start()
    {
        inst = this;
        message.SetActive(false);
    }

    IEnumerator conseguirPreguntas()
    {
        string url = "https://tutor-api.hacklab.org.bo/api/preguntas/" + (Player.inst.NivelActual);
        UnityWebRequest request = UnityWebRequest.Get(url);
        request.SetRequestHeader("Authorization", "Bearer " + Player.inst.UserToken);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            MenuManagerQuiz.inst.mostrarMensaje(0, "No se pudo obtener el mensaje");
            Debug.LogError("Error: " + request.error);
        }
        else
        {
            data = JsonUtility.FromJson<ResponseData>(request.downloadHandler.text);
            setupScena(data);
        }
    }
    void OnDisable()
    {
        Player.OnActionQuiz -= OnActionStartQuiz;
        RespuestasQuiz.OnActionCompletedUserHandler -= completedScene;
        RespuestasQuiz.OnActionRespUserMessage -= mostrarMensaje;
        PreguntasEtapa0.OnActionImagenPregunta -= mostrarMensaje;
    }
    void OnEnable()
    {
        Player.OnActionQuiz += OnActionStartQuiz;
        RespuestasQuiz.OnActionCompletedUserHandler += completedScene;
        RespuestasQuiz.OnActionRespUserMessage += mostrarMensaje;
        PreguntasEtapa0.OnActionImagenPregunta += mostrarMensaje;
    }
    void completedScene()
    {
        endQuiz.SetActive(false);
        escena1.SetActive(false);
        escena2.SetActive(false);
        escena3.SetActive(true);
    }

    void OnActionStartQuiz()
    {
        if (Player.inst.Estado == "COMPLETED") { completedScene(); }
        else { StartCoroutine(conseguirPreguntas()); }
    }
    public void setupScena(ResponseData data)
    {
        nivelInfo.GetComponentInChildren<TextMeshProUGUI>().text = Player.inst.NombreLeccionActual;
        if (Player.inst.NivelActual < 4)
        {
            escena1.SetActive(true);
            escena3.SetActive(false);
            escena1.GetComponent<PreguntasEtapa0>().submitAnswer(data);
            escena2.SetActive(false);
        }
        else
        {
            escena3.SetActive(false);
            escena1.SetActive(false);
            escena2.SetActive(true);
            escena2.GetComponent<PreguntasEtapa1>().submitAnswer(data);
        }
        map.SetActive(false);
    }
    public void btnContinuarLeccion()
    {
        escena1.SetActive(false);
        escena2.SetActive(false);
        OnActionStartQuiz();
    }
   

    public void mostrarMensaje(int code, string message)
    {
        switch (code)
        {
            case 0:
                this.message.SetActive(true);
                this.message.GetComponentInChildren<MensajePrompt>().warningMessage(message);
                break;
            case 1:
                this.message.SetActive(true);
                this.message.GetComponentInChildren<MensajePrompt>().successMessage(message);
                break;
            case 2:
                this.message.SetActive(true);
                this.message.GetComponentInChildren<MensajePrompt>().infoMessage(message);
                break;
            default:
                break;
        }
    }
}
