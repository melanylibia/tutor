
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.VisualScripting;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/LecturaScriptableObject", order = 1)]

public class LecturaScriptableObject : ScriptableObject
{
    [SerializeField]
    public List<Lectura> lecturas;
}
