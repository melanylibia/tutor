using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
public class RespuestasQuiz : MonoBehaviour
{
    public static RespuestasQuiz inst;
    void Start()
    {
        inst = this;
    }
    [Serializable]
    public class RespuestaData
    {
        public string preguntasCorrectas;
        public string tiempoLeccion;
    }
    [Serializable]
    public class ResponseRequestData
    {
        public string finalizado;
        public string mensaje;
        public AvanceData datos;
    }
    [Serializable]
    public class AvanceData
    {
        public bool avance;
        public UsuarioUpdatedData usuario;
    }
    [Serializable]
    public class UsuarioUpdatedData
    {
        public string estado;
        public string idLeccion;
        public string leccion;
    }
    // Response: {"finalizado":true,"mensaje":"Registro creado con �xito.","datos":{"avance":true}}
    public static event Action<int, string> OnActionUpdatedUserHandler;
    public static event Action OnActionCompletedUserHandler;
    public static event Action<int, string> OnActionRespUserMessage;
    IEnumerator responderQuiz(string nuevoPreguntasCorrectas, string nuevoTiempoLeccion)
    {
        string url = "https://tutor-api.hacklab.org.bo/api/respuestas/respuestasLeccion";

        RespuestaData requestData = new RespuestaData
        {
            preguntasCorrectas = nuevoPreguntasCorrectas,
            tiempoLeccion = nuevoTiempoLeccion,
        };

        string jsonData = JsonUtility.ToJson(requestData);
        UnityWebRequest request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(jsonData);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + Player.inst.UserToken);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
        {
            if (OnActionRespUserMessage != null)
            {
                OnActionRespUserMessage.Invoke(0, "No fue posible enviar las respuestas del lección");
            }
            Debug.LogError("Error: " + request.error);
        }
        else
        {
            ResponseRequestData response = JsonUtility.FromJson<ResponseRequestData>(request.downloadHandler.text);
            if (response.datos.avance)
            {
                if (response.datos.usuario.estado == "COMPLETED")
                {
                    Player.inst.Estado = "COMPLETED";
                    OnActionCompletedUserHandler.Invoke();
                }
                else
                {
                    if (OnActionUpdatedUserHandler != null)
                    {
                        OnActionUpdatedUserHandler.Invoke(int.Parse(response.datos.usuario.idLeccion), response.datos.usuario.leccion);
                    }
                }
            }
        }
    }

    public void endQuiz(string nuevoPreguntasCorrectas, string nuevoTiempoLeccion)
    {
        StartCoroutine(responderQuiz(nuevoPreguntasCorrectas, nuevoTiempoLeccion));
    }
}
