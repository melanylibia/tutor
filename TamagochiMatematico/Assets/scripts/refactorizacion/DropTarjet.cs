using UnityEngine.EventSystems;
using UnityEngine;

public class DropTarjet : MonoBehaviour, IDropHandler
{
    
    public void OnDrop(PointerEventData eventData)
    {
        if(eventData.pointerDrag!=null){
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
            Options optionItem = eventData.pointerDrag.GetComponent<Options>();
            if(optionItem!=null){
                optionItem.submit();
                optionItem.gameObject.transform.position =  optionItem.PosicionInicial.position;
            }
        }
    }
}
