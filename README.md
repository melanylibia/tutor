# Mate tutor

Es juego donde se debe alimentar a la zanahoria:

Niveles:

1. Porcentaje
2. Tributos
3. Impuestos
4. Capital Final
5. Capital Final 2
6. Interes Simple
7. Interes Simple 2

**Ejemplo ejercicio:**

$`\frac{20}{5} = `$

$`100\% - 20\% = 80\% \\`$

$`\frac{20}{5} = 4`$

## Prerequisitos: 

Tener una cuenta creada de jugador, donde el administrador del juego debe crear la cuenta del jugador.

## Stack

- C#

- NestJS

- Nginx

- Minio

- Postgresql

## Installation

1. git clone https://gitlab.com/melanylibia/tutor.git

2. Inside Unity open Assets

## MateTutor


<img src="https://gitlab.com/melanylibia/tutor/raw/main/imgs/login.jpg" alt="Image Login" width="150">

<img src="https://gitlab.com/melanylibia/tutor/raw/main/imgs/juego.jpg" alt="Image Login" width="150">











