using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mapa : MonoBehaviour
{
    
    List<string> listLevels;
    void Awake()
    {
        llenarLista();
    }
    public void llenarLista()
    {
        listLevels = new List<string>();
        listLevels.Add("Porcentaje");
        listLevels.Add("Tipos_de_impuestos");
        listLevels.Add("Tributaria_concepto_basicos");
        listLevels.Add("Capital_Final_I");
        listLevels.Add("Capital_Final_II");
        listLevels.Add("Intereses_Generados_I");
        listLevels.Add("Intereses_Generados_II");
        listLevels.Add("Intereses_Generados_II");
    }
    void OnEnable()
    {
        llenarLista();
        int index = 0;
        Transform parentTransform = transform;
        for (int i = 0; i < parentTransform.childCount; i++)
        {
            Transform child = parentTransform.GetChild(i);
            child.gameObject.name = listLevels[index];
            if (Player.inst.NombreLeccionActual == listLevels[index].Replace("_", " "))
            {
                child.Find("back").gameObject.SetActive(true);
            }
            else
            {
                child.Find("back").gameObject.SetActive(false);
            }
            index++;
            child.gameObject.SetActive(true);
        }
    }
    void OnDisable()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }
}
