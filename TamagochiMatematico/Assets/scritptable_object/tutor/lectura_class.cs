using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [System.Serializable]
    public class Lectura
    {
        public string contenido;
        public Sprite image;
        public Lectura(string contenido, Sprite image)
        {
            this.contenido = contenido;
            this.image = image;
        }
    }

