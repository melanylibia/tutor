using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class TestInicial : MonoBehaviour
{


    public static TestInicial testInicial;
    [System.Serializable]
    public class _OptionTI
    {
        public string texto;
        public int value;
    }

    [System.Serializable]
    public class QuestionTI
    {
        public string question;
        public _OptionTI[] options;
    }

    [System.Serializable]
    public class QuizTI
    {
        public QuestionTI[] quiz;
    }

    public GameObject obj_pregunta;
    int total = 0;

    private string idUsuario;
    public string IdUsuario
    {
        get { return idUsuario; }
        set { idUsuario = value; }
    }
    int index = 0;
    public List<Button> opciones;
    void clearRepaso()
    {
        foreach (Button opcion in opciones)
        {
            opcion.gameObject.SetActive(false);
            opcion.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = "";
        }
    }
    [System.Serializable]
    public class TestRespuestaData
    {
        public bool pruebaRealizada;
        public string total;
    }

    private void Awake()
    {
        if (testInicial == null)
            testInicial = this;
    }
    QuizTI quiz;
    void Start()
    {
        clearRepaso();
        string jsonString = @"{
            ""quiz"": [
                {""question"": ""¿Cuál de las siguientes NO es una variable en la fórmula para calcular el interés compuesto?"",
                ""options"": [
                {""texto"":""a) El tiempo (t)"", ""value"": 3},
                {""texto"":""b) La tasa de interés (r)"", ""value"": 2},
                {""texto"":""c) La cantidad inicial (P)"", ""value"": 1},
                {""texto"":""d) La cantidad acumulada después de cierto tiempo (A)"", ""value"": 0}
                ]},
                {""question"": ""Si tienes un recipiente lleno hasta la mitad con jugo de naranja y bebes 1/4 del contenido, ¿qué fracción del recipiente queda llena?""
                ,
                ""options"": [
                {""texto"":""a) 3/8"", ""value"": 3},
                {""texto"":""b) 1/2"", ""value"": 2},
                {""texto"":""c) 3/4"", ""value"": 1}
                ]},
                {""question"": ""Si x representa un número desconocido, y sabemos que x+5=12, ¿cuál es el valor de x?""
                ,
                ""options"": [
                {""texto"":""a) 5"", ""value"": 3},
                {""texto"":""b) 7"", ""value"":  2},
                {""texto"":""c) 12"", ""value"": 1},
                {""texto"":""d) 17"", ""value"": 0}
                ]},
                {""question"": ""Si Ana tiene 5 libros y su amigo Pedro le presta 3 libros más, ¿cuántos libros tendrá Ana en total?"",
                ""options"": [
                {""texto"":""a) 5"", ""value"": 3},
                {""texto"":""b) 8"", ""value"": 2},
                {""texto"":""c) 3"", ""value"": 1},
                {""texto"":""d) 2"", ""value"": 0}
                ]},
                {""question"": "" Si una piña cuesta $3. ¿Cuánto costará la docena y media de piñas?"",
                ""options"": [
                {""texto"":""a) 54"", ""value"": 3},
                {""texto"":""b) 36"", ""value"": 2},
                {""texto"":""c) 50"", ""value"": 1},
                {""texto"":""d) 72"", ""value"": 0}
                ]}]
        }";

        quiz = JsonUtility.FromJson<QuizTI>(jsonString);
        siguientePregunta();
    }
    public void siguientePregunta()
    {
        obj_pregunta.GetComponent<TextMeshProUGUI>().text = quiz.quiz[index].question;
        int indxOpt = 0;
        foreach (_OptionTI o in quiz.quiz[index].options)
        {
            opciones[indxOpt].gameObject.GetComponent<OptionTI>()._Value = o.value;
            opciones[indxOpt].gameObject.SetActive(true);
            opciones[indxOpt].gameObject.GetComponentInChildren<TextMeshProUGUI>().text = o.texto;
            indxOpt++;
        }
    }
    public static event Action<TestRespuestaData, string> OnActionSubmitTI;
    public void valorOption(int value)
    {
        clearRepaso();
        total += value;
        if (index < quiz.quiz.Length - 1)
        {
            index++;
            siguientePregunta();
        }
        else
        {
            TestRespuestaData testData = new TestRespuestaData();
            testData.pruebaRealizada = true;
            testData.total = total.ToString();
            obj_pregunta.GetComponent<TextMeshProUGUI>().text = "Finalizado";
            // TestInicial.TestRespuestaData
            if (OnActionSubmitTI != null)
            {
                OnActionSubmitTI.Invoke(testData, idUsuario);
            }
            //££ adicionar coroutina
            // SceneManager.LoadScene(2);
        }
    }
}
//TestInicial.TestRespuestaData



