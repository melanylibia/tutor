using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnMapa : MonoBehaviour
{
    public GameObject prompSalir;
    public GameObject prompRepaso;
    void Start()
    {

    }
    private void OnEnable()
    {
        this.gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(cambiarEscena);
    }
    void cambiarEscena()
    {
        prompSalir.SetActive(true);
        prompRepaso.SetActive(false);
    }
}
